#!/bin/bash
# Setup Jenkins Project
if [ "$#" -ne 3 ]; then
    echo "Usage:"
    echo "  $0 GUID REPO CLUSTER"
    echo "  Example: $0 wkha https://github.com/wkulhanek/ParksMap na39.openshift.opentlc.com"
    exit 1
fi

GUID=$1
REPO=$2
CLUSTER=$3


oc new-app jenkins-persistent --param MEMORY_LIMIT=2Gi --param VOLUME_CAPACITY=4Gi -n ${GUID}-jenkins

oc new-build --name=jenkins-slave-maven-appdev --dockerfile="$(< ./Infrastructure/docker/skopeo/Dockerfile)" -n ${GUID}-jenkins

oc get pod -n ${GUID}-jenkins | grep 'slave' | grep "Completed"

while : ; do

oc get pod -n ${GUID}-jenkins | grep 'slave' | grep "Completed"

    if [ $? == "0" ]
      then
        echo 'jenkins-slave-maven-appdev build completed'
        break
      else
        echo '...checking if the slave is up....'
        sleep 10
    fi


#pipelines
oc process -f Infrastructure/templates/jenkins/pipeline-template.json -n ${GUID}-jenkins -p GIT_URI=${REPO} -p NAME=mlbparks -p JENKINSFILE_PATH=MLBParks/Jenkinsfile | oc create -f -
oc process -f Infrastructure/templates/jenkins/pipeline-template.json -n ${GUID}-jenkins -p GIT_URI=${REPO} -p NAME=nationalparks -p JENKINSFILE_PATH=Nationalparks/Jenkinsfile | oc create -f -
oc process -f Infrastructure/templates/jenkins/pipeline-template.json -n ${GUID}-jenkins -p GIT_URI=${REPO} -p NAME=parksmap -p JENKINSFILE_PATH=ParksMap/Jenkinsfile | oc create -f -






